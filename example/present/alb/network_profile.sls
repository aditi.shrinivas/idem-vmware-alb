resource_is_present:
    nsx_alb.alb.network_profile.present:
    - name: idem-nw1
      profile:
        type: PROTOCOL_TYPE_UDP_FAST_PATH
        udp_fast_path_profile:
            per_pkt_loadbalance: false
            session_idle_timeout: 10
            snat: true
