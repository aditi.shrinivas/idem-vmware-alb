resource_is_present:
    nsx_alb.alb.ssl_profile.present:
    - accepted_ciphers: >
        ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES128-SHA:ECDHE-ECDSA-AES256-SHA

      accepted_versions:
        - type: SSL_VERSION_TLS1
        - type: SSL_VERSION_TLS1_1
        - type: SSL_VERSION_TLS1_2
      cipher_enums:
        - TLS_ECDHE_ECDSA_WITH_AES_128_GCM_SHA256
        - TLS_ECDHE_ECDSA_WITH_AES_128_CBC_SHA

      name: idem-ssl1
      send_close_notify: false
      ssl_rating:
        compatibility_rating: SSL_SCORE_EXCELLENT
        performance_rating: SSL_SCORE_EXCELLENT
        security_score: '100.0'
